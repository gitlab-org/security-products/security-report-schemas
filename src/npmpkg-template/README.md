# Schemas for GitLab security reports

This package maintains the schema definitions for the security reports emitted by GitLab security scanners. It defines the reports for:
- [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
- [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/)
- [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/)
- [Dynamic Application Security Testing](https://docs.gitlab.com/ee/user/application_security/dast/)
- [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
- [Coverage Guided Fuzzing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/)

The schemas are defined using [JSON Schema](https://json-schema.org/). Any security scanner that integrates into GitLab must produce a JSON report that adheres to one of above schemas.

More information about the how and why of the schemas can be found by watching the [Security Report Format Brown Bag Session](https://youtu.be/DqKsdNLXxes).

## Schema Versioning

This package follows the [SchemaVer](https://github.com/snowplow/iglu/wiki/SchemaVer) standard `MODEL.REVISION.ADDITION` to version JSON schemas. The package version mirrors the current standard version for all schemas.

### Handling Updates

Review the [CHANGELOG](${project_url}/-/blob/v${pkg_version}/CHANGELOG.md) to read through the latest changes in the schema definitions.

### Additional Properties

Secure schemas allow for additional properties to be present in JSON files. This means that the schemas are only concerned with fields in a Secure Report that are defined by the schema. The presence of any additional fields will not cause validation to fail.

This is useful for products that produce Secure Reports:

- Experimental fields can be added to a Secure Report, without affecting how the report is used.
- It allows the product to be ahead of the Secure Report Format, when the product team is confident new fields will be merged into the schemas.

Any additional properties added to a Secure Report are considered experimental and may not be supported. For this reason, adding optional fields to the Secure Report Format is considered an `ADDITION`, not a `REVISION` change.

## Bugs & Contributing
If you find a bug, please report it to our issues queue at [gitlab.org/.../security-report-schemas/issues](${project_url}/-/issues)

If you want to help and extend the list of supported scanners, read the contribution guidelines [gitlab.org/.../security-report-schemas/CONTRIBUTING.md](${project_url}/-/blob/master/CONTRIBUTING.md)
