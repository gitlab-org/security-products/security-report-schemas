# JSON Schema Meta-Validator

This is a simple Go program used to validate that the JSON schemas in this repository
comply with [the specification](https://github.com/json-schema-org/json-schema-spec).

## Compilation

CI tests use the binaries checked into the repository.
If the validator is changed, then the binaries need to be re-built.
You can re-compile the binaries by running `sh ./bin/validate-src/build.sh`.
