#!/usr/bin/env bash

set -eo pipefail

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$PROJECT_DIRECTORY/scripts/changelog-utils.sh"

log() {
  printf "\n\n######### %s #########\n" "$*" >>/dev/stdout
}

VERSION="$(changelog_last_version)"
CI_API_V4_URL=${CI_API_V4_URL:-"https://gitlab.com/api/v4"}
DIST_PROJECT_ID=${DIST_PROJECT_ID:-"37954006"} # gitlab-org/ruby/gems/gitlab-security_report_schemas
DIST_PROJECT_REF="main"

trigger_downstream_gem_pipeline() {
  log "Triggering downstream distribution pipeline for version $VERSION"

  local trigger_url="$CI_API_V4_URL/projects/$DIST_PROJECT_ID/trigger/pipeline"

  curl --fail \
    --silent \
    --show-error \
    --request POST \
    --form "token=$PIPELINE_TRIGGER_TOKEN" \
    --form "ref=$DIST_PROJECT_REF" \
    --form "variables[ADD_SCHEMA_VERSION]=$VERSION" "$trigger_url" | tee trigger.json

  log "Downstream pipeline triggered successfully"
  echo "See $(jq -r '.web_url' <trigger.json)."
}

trigger_downstream_gem_pipeline
