import b from './builders/index'
import {schemas} from './support/schemas'

describe('cluster image scanning schema', () => {

  it('should validate location', () => {
    const report = b.cluster_image_scanning.report({
      vulnerabilities: [b.cluster_image_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
          kubernetes_resource: {
            namespace: 'production',
            kind: 'Deployment',
            container_name: 'debian',
            name: 'debian-deployment'
          }
        }
      })]
    })

    expect(schemas.cluster_image_scanning.validate(report).success).toBeTruthy()
  })

  it('location dependency is required', () => {
    const report = b.cluster_image_scanning.report({
      vulnerabilities: [b.cluster_image_scanning.vulnerability({
        location: {
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
          kubernetes_resource: {
            namespace: 'production',
            kind: 'Deployment',
            container_name: 'debian',
            name: 'debian-deployment'
          }
        }
      })]
    })

    expect(schemas.cluster_image_scanning.validate(report).errors[0].message).toContain('must have required property \'dependency\'')
  })

  it('location image is required', () => {
    const report = b.cluster_image_scanning.report({
      vulnerabilities: [b.cluster_image_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          kubernetes_resource: {
            namespace: 'production',
            kind: 'Deployment',
            container_name: 'debian',
            name: 'debian-deployment'
          }
        }
      })]
    })

    expect(schemas.cluster_image_scanning.validate(report).errors[0].message).toContain('must have required property \'image\'')
  })

  it('location kubernetes resource is required', () => {
    const report = b.cluster_image_scanning.report({
      vulnerabilities: [b.cluster_image_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
        }
      })]
    })

    expect(schemas.cluster_image_scanning.validate(report).errors[0].message).toContain('must have required property \'kubernetes_resource\'')
  })

  it('requires package', () => {
    const report = b.cluster_image_scanning.report({
      vulnerabilities: [b.cluster_image_scanning.vulnerability({
        location: {
          dependency: {
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
          kubernetes_resource: {
            namespace: 'production',
            kind: 'Deployment',
            container_name: 'debian',
            name: 'debian-deployment'
          }
        }
      })]
    })

    expect(schemas.cluster_image_scanning.validate(report).errors[0].message).toContain('must have required property \'package\'')
  })

  it('requires version', () => {
    const report = b.cluster_image_scanning.report({
      vulnerabilities: [b.cluster_image_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
          kubernetes_resource: {
            namespace: 'production',
            kind: 'Deployment',
            container_name: 'debian',
            name: 'debian-deployment'
          }
        }
      })]
    })

    expect(schemas.cluster_image_scanning.validate(report).errors[0].message).toContain('must have required property \'version\'')
  })

  it('requires name', () => {
    const report = b.cluster_image_scanning.report({
      vulnerabilities: [b.cluster_image_scanning.vulnerability({
        location: {
          dependency: {
            package: {noname: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
          kubernetes_resource: {
            namespace: 'production',
            kind: 'Deployment',
            container_name: 'debian',
            name: 'debian-deployment'
          }
        }
      })]
    })

    expect(schemas.cluster_image_scanning.validate(report).errors[0].message).toContain('must have required property \'name\'')
  })

})
