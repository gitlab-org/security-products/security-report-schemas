import {merge} from '../../support/merge'

const defaults = {
  analyzer: {
    id: 'test_analyzer_id',
    name: 'Test Analyzer name',
    vendor: {
      name: "Test Vendor"
    },
    version:"0.1.0"
  },
  scanner: {
    id: 'gitleaks',
    name: 'Gitleaks',
    url: 'https://github.com/zricethezav/gitleaks',
    vendor: {
      name: 'GitLab'
    },
    version: '3.4.1'
  },
  type: 'secret_detection',
  start_time: '2020-09-18T20:06:51',
  end_time: '2020-09-18T20:07:01',
  status: 'success'
}

export const scan = (scanOverrides) => {
  return merge(defaults, scanOverrides)
}
