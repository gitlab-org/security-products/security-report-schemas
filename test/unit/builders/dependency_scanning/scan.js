import {merge} from '../../support/merge'

const defaults = {
  analyzer: {
    id: 'test_analyzer_id',
    name: 'Test Analyzer name',
    vendor: {
      name: "Test Vendor"
    },
    version:"0.1.0"
  },
  scanner: {
    id: 'gemnasium',
    name: 'gemnasium',
    url: 'https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium',
    vendor: {
      name: 'GitLab'
    },
    version: 'v2.37.2',
  },
  start_time: '2020-12-04T03:26:07',
  end_time: '2020-12-04T03:26:19',
  type: 'dependency_scanning',
  status: 'success'
}

export const scan = (scanOverrides) => {
  return merge(defaults, scanOverrides)
}
