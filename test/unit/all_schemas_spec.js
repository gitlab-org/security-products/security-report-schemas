import b from './builders/index'
import {schemas} from './support/schemas'
import {withVulnerabilityDetails} from './builders/vulnerability_details/vulnerability'
import { error } from 'ajv/dist/vocabularies/applicator/dependencies'

const schemaTypes = [
  {name: 'dast', builder: b.dast, schema: schemas.dast},
  {name: 'sast', builder: b.sast, schema: schemas.sast},
  {name: 'container_scanning', builder: b.container_scanning, schema: schemas.container_scanning},
  {name: 'coverage_fuzzing', builder: b.coverage_fuzzing, schema: schemas.coverage_fuzzing},
  {name: 'secret_detection', builder: b.secret_detection, schema: schemas.secret_detection},
  {name: 'dependency_scanning', builder: b.dependency_scanning, schema: schemas.dependency_scanning}
]

schemaTypes.forEach((type) => {
  describe(`common[${type.name}] schema`, () => {
    describe('when ftp and http schemes are used for identifiers[].url', () => {
      it('passes validation', () => {
        const report = type.builder.report({
          vulnerabilities: [type.builder.vulnerability({
            identifiers: [{
              type: "type",
              name: "name",
              value: "value",
              url: "https://cwe.mitre.org/data/definitions/362.html"
            },
            {
              type: "type",
              name: "name",
              value: "value",
              url: "ftp://cwe.mitre.org/data/definitions/362.html"
            }]
          })]
        })

        expect(type.schema.validate(report).success).toBeTruthy();
      })
    })

    describe('identifiers', () => {
      describe('when there are no identifiers', () => {
        const report = type.builder.report({
          vulnerabilities: [type.builder.vulnerability({
            identifiers: []
          })]
        })

        it('fails validation', () => {
          expect(type.schema.validate(report).success).toBeFalsy();
        })

        it('includes an error message', () => {
          expect(type.schema.validate(report).errors[0].message).toContain('must NOT have fewer than 1 items')
        })
      })

      describe('when identifiers[].type is empty', () => {
        const report = type.builder.report({
          vulnerabilities: [type.builder.vulnerability({
            identifiers: [{
              type: "",
              name: "name",
              value: "value"
            }]
          })]
        })

        it('fails validation', () => {
          expect(type.schema.validate(report).success).toBeFalsy();
        })

        it('includes an error message', () => {
          expect(type.schema.validate(report).errors[0].message).toContain('must NOT have fewer than 1 characters')
        })
      })

      describe('when identifiers[].name is empty', () => {
        const report = type.builder.report({
          vulnerabilities: [type.builder.vulnerability({
            identifiers: [{
              type: "type",
              name: "",
              value: "value"
            }]
          })]
        })

        it('fails validation', () => {
          expect(type.schema.validate(report).success).toBeFalsy();
        })

        it('includes an error message', () => {
          expect(type.schema.validate(report).errors[0].message).toContain('must NOT have fewer than 1 characters')
        })
      })

      describe('when identifiers[].value is empty', () => {
        const report = type.builder.report({
          vulnerabilities: [type.builder.vulnerability({
            identifiers: [{
              type: "type",
              name: "name",
              value: ""
            }]
          })]
        })

        it('fails validation', () => {
          expect(type.schema.validate(report).success).toBeFalsy();
        })

        it('includes an error message', () => {
          expect(type.schema.validate(report).errors[0].message).toContain('must NOT have fewer than 1 characters')
        })
      })
    })

    describe('report', () => {
      describe('when version is empty', () => {
        const report = type.builder.report({version: undefined})

        it('fails validation', () => {
          expect(type.schema.validate(report).success).toBeFalsy();
        })

        it('includes an error message', () => {
          expect(type.schema.validate(report).errors[0].message).toContain('must have required property \'version\'')
        })
      })

      describe('when vulnerabilities are empty', () => {
        const report = type.builder.report({vulnerabilities: []})

        it('passes validation', () => {
          expect(type.schema.validate(report).success).toBeTruthy()
        })
      })

      describe('when vulnerabilities have additional details', () => {
        const report = withVulnerabilityDetails(type.builder.report());

        it('passes validation', () => {
          expect(type.schema.validate(report).success).toBeTruthy();
        });

        const invalid_file_location_report = withVulnerabilityDetails(type.builder.report(), {  
          "invalid_location": {
            "name": "invalid file-location test",
            "type": "file-location",
            "file_name": "new_file.c",
            "line_start": 0, // invalid, minimum 1.
            "column_end": 3  // column_start is missing when column_end exist
        }});

        it('fails validation and includes error message', () => {
          expect(type.schema.validate(invalid_file_location_report).success).toBeFalsy();
          const error_messages = type.schema.validate(invalid_file_location_report).errors.map(error => error.message);
          expect(error_messages).toContain("must be >= 1");
          expect(error_messages).toContain("must have property column_start when property column_end is present");
      })})

      describe('when vulnerabilities[].tracking contains the required fields', () => {
        const report = type.builder.report({
          vulnerabilities: [type.builder.vulnerability({
            tracking: {
              type: "source",
              items: [{
                file: "app/controllers/groups_controller.rb",
                start_line: 6,
                end_line: 6,
                signatures: [{
                  "algorithm": "scope_offset",
                  "value": "app/controllers/groups_controller.rb|GroupsController[0]|new_group[0]:4"
                }]
              }]
            }
          })]
        })

        it('passes validation', () => {
          expect(type.schema.validate(report).success).toBeTruthy();
        })
      })

      describe('when vulnerabilities[].flags contains the required fields', () => {
        const report = type.builder.report({
          vulnerabilities: [type.builder.vulnerability({
            flags: [{
              type: "flagged-as-likely-false-positive",
              origin: "post analyzer X",
              description: "static string to sink"
            },{
              type: "flagged-as-likely-false-positive",
              origin: "post analyzer Y",
              description: "integer to sink"
            }]
          })]
        })

        it('passes validation', () => {
          expect(type.schema.validate(report).success).toBeTruthy();
        })
      })
    })

    it('complies to the JSON specification', () => {
      const report = type.builder.report()
      expect(schemas.json_draft_07.validate(report).success).toBeTruthy()
    })

    describe('scan.options', () => {
      describe('when scan.options is invalid', () => {
        const report = type.builder.report({
          scan: type.builder.scan({
            options: [
              { /* name < minLength */ value: "valid value", name: "" },
              { /* name > maxLength */ value: "valid value", name: "THIS_NAME_IS_LONGER_THAN_THE_LEGAL_LIMIT_FOR_SCAN_OPTIONS_SINCE_THE_CHARACTER_LIMIT_IS_ONLY_TWO_HUNDRED_FIFTY_FIVE_CHARACTERS_BUT_THIS_IS_A_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_VERY_LONG_STRING" },
              { name: "source_empty", source: "", value: "valid value" },
              { name: "undefined_enum_source", source: "invalid source", value: "valid value" },
              { name: "value_array", value: [] },
              { name: "value_object", value: {} },
              { name: null, value: "valid value" },
            ]
          })
        })

        it('fails validation and includes error messages', () => {
          expect(type.schema.validate(report).success).toBeFalsy();
          expect(type.schema.validate(report).errors.length).toBe(7)
          expect(type.schema.validate(report).errors[0].message).toContain('must NOT have fewer than 1 characters')
          expect(type.schema.validate(report).errors[1].message).toContain('must NOT have more than 255 characters')
          expect(type.schema.validate(report).errors[2].message).toContain('must be equal to one of the allowed values')
          expect(type.schema.validate(report).errors[3].message).toContain('must be equal to one of the allowed values')
          expect(type.schema.validate(report).errors[4].message).toContain('must be boolean,integer,null,string')
          expect(type.schema.validate(report).errors[5].message).toContain('must be boolean,integer,null,string')
          expect(type.schema.validate(report).errors[6].message).toContain('must be string')
        })
      })

      describe('when scan.options is null', () => {
        const report = type.builder.report({
          scan: type.builder.scan({
            options: null
          })
        })

        it('fails validation', () => {
          expect(type.schema.validate(report).success).toBeFalsy();
        })

        it('includes an error message', () => {
          expect(type.schema.validate(report).errors[0].message).toContain('must be array')
        })
      })

      describe('when scan.options is an empty array', () => {
        const report = type.builder.report({
          scan: type.builder.scan({
            options: []
          })
        })

        it('passes validation', () => {
          expect(type.schema.validate(report).success).toBeTruthy();
        })
      })

      describe('when scan.options is undefined', () => {
        const report = type.builder.report({
          scan: type.builder.scan({
            options: undefined
          })
        })

        it('passes validation', () => {
          expect(type.schema.validate(report).success).toBeTruthy();
        })
      })
    })

    describe('when scan.options contains the required fields', () => {
      const report = type.builder.report({
        scan: type.builder.scan({
          options: [
            { name: "bool", source: "argument", value: true },
            { name: "empty_string", source: "env_variable", value: "" },
            { name: "int", source: "env_variable", value: 2 },
            { name: "nullable", source: "other", value: null },
            { name: "string", source: "file", value: "fatal" },
          ]
        })
      })

      it('passes validation', () => {
        expect(type.schema.validate(report).success).toBeTruthy();
      })
    })

    describe('when scan.analyzer contains the required fields', () => {
      const report = type.builder.report({
        scan: type.builder.scan({
          analyzer: {
            id: 'gitlab-dast',
            name: 'GitLab DAST',
            url: 'https://gitlab.com/dast',
            version: '1.6.20',
            vendor: {
              name: 'GitLab'
            }
          }
        })
      })

      it('passes validation', () => {
        expect(type.schema.validate(report).success).toBeTruthy();
      })
    })

    describe('vulnerability', () => {
      describe('when vulnerability.name exceeds maxLength', () => {
        const report = type.builder.report({
          vulnerabilities: [type.builder.vulnerability({
            name: 'n'.repeat(255+1)
          })]
        })

        it('fails validation', () => {
          expect(type.schema.validate(report).success).toBeFalsy();
        })

        it('includes an error message', () => {
          expect(type.schema.validate(report).errors[0].message).toContain('must NOT have more than 255 characters')
        })
      })

      describe('when vulnerability.description exceeds maxLength', () => {
        const report = type.builder.report({
          vulnerabilities: [type.builder.vulnerability({
            description: 'd'.repeat(1048576+1)
          })]
        })

        it('fails validation', () => {
          expect(type.schema.validate(report).success).toBeFalsy();
        })

        it('includes an error message', () => {
          expect(type.schema.validate(report).errors[0].message).toContain('must NOT have more than 1048576 characters')
        })
      })

      describe('when vulnerability.solution exceeds maxLength', () => {
        const report = type.builder.report({
          vulnerabilities: [type.builder.vulnerability({
            solution: 's'.repeat(7000+1)
          })]
        })

        it('fails validation', () => {
          expect(type.schema.validate(report).success).toBeFalsy();
        })

        it('includes an error message', () => {
          expect(type.schema.validate(report).errors[0].message).toContain('must NOT have more than 7000 characters')
        })
      })
    })

    describe('cvss_vectors', () => {
      describe('when cvss_vectors[].vector matches the specification regex', () => {
        const report = type.builder.report({
          vulnerabilities: [type.builder.vulnerability({
            cvss_vectors: [{
              vendor: 'VendorA',
              vector: 'CVSS:3.1/AV:P/AC:H/PR:H/UI:R/S:C/C:H/I:H/A:H/E:H/RL:U/RC:C/CR:H/IR:H/AR:H/MAV:P/MAC:H/MPR:H/MUI:R/MS:C/MC:L/MI:H/MA:H'
            }, {
              vendor: 'VendorB',
              vector: 'CVSS:3.0/AV:P/AC:H/PR:H/UI:R/S:C/C:H/I:H/A:H/E:H/RL:U/RC:C/CR:H/IR:H/AR:H/MAV:P/MAC:H/MPR:H/MUI:R/MS:C/MC:L/MI:H/MA:H'
            }, {
              vendor: 'VendorC',
              vector: 'AV:N/AC:L/Au:N/C:C/I:C/A:C/E:H/RL:U/RC:C/CDP:H/TD:H/CR:H/IR:H/AR:H'
            }]
          })]
        })

        it('passes validation', () => {
          expect(type.schema.validate(report).success).toBeTruthy()
        })
      })

      describe('when cvss_vectors[].vector does not match the specification regex', () => {
        const report = type.builder.report({
          vulnerabilities: [type.builder.vulnerability({
            cvss_vectors: [{
              vendor: 'VendorA',
              vector: 'THIS_IS_NOT_A_VALID_VECTOR'
            }]
          })]
        })

        it('fails validation', () => {
          expect(type.schema.validate(report).success).toBeFalsy();
        })

        it('includes an error message', () => {
          expect(type.schema.validate(report).errors[0].message).toContain('must match pattern')
        })
      })
    })

    describe('remedations', () => {
      describe('when required remediation properties are present', () => {
        const report = type.builder.report({
          remediations: [
            {
              fixes: [{id: 'id'}, {id: 'other-id'}],
              summary: 'Remediation summary',
              diff: 'a diff'
            }
          ]
        })

        it('passes validation', () => {
          expect(type.schema.validate(report).success).toBeTruthy();
        })
      })

      describe('when remediations[].fixes[].id is missing', () => {
        const report = type.builder.report({
          remediations: [
            {
              fixes: [{cve: 'some-cve'}],
              summary: 'Remediation summary',
              diff: 'a diff'
            }
          ]
        })

        it('fails validation', () => {
          expect(type.schema.validate(report).success).toBeFalsy();
        })

        it('includes an error message', () => {
          expect(type.schema.validate(report).errors[0].message).toContain('must have required property \'id\'')
        })
      })
    })
  })
})
